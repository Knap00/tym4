module cz.vse.menumovers {
    requires javafx.controls;
    requires javafx.fxml;


    opens cz.vse.menumovers to javafx.fxml;
    exports cz.vse.menumovers;
}